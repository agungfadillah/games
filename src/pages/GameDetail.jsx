import { Container, Row, Col, Nav, NavItem } from "react-bootstrap";
import "../assets/css/Games.css";
import { useEffect, useState } from "react";
import back from "../assets/images/back.png";
import logo from "../assets/images/logo1.png";
import imgbatu from "../assets/images/batu.png";
import imggunting from "../assets/images/gunting.png";
import imgkertas from "../assets/images/kertas.png";
import refresh from "../assets/images/refresh.png";

export default function GameDetail() {
  const [com, setCom] = useState("");
  const [player, setPlayer] = useState("");
  const [hasil, setHasil] = useState("");

  useEffect(() => {
    if (player && com) {
      if (player === com) {
        setHasil("draw");
      } else if ((player === "batu" && com === "gunting") || (player === "kertas" && com === "batu") || (player === "gunting" && com === "kertas")) {
        setHasil("player win");
      } else {
        setHasil("com win");
      }
    }
  }, [player, com]);

  
    const handlePicks = (pick) => {
        const picks = ["batu", "gunting", "kertas"];
        const computerPicks = picks[Math.floor(Math.random() * picks.length)];
        setPlayer(pick);
        document.getElementById(pick).classList.add("picked");
        setCom(computerPicks);
        if (computerPicks === "batu") {
        document.getElementById("batu2").style.background=
        "#C4C4C4";
        document.getElementById("gunting2").style.background=
        "#9C835F";
        document.getElementById("kertas2").style.background=
        "#9C835F";
        }
        else if (computerPicks === "gunting") {
        document.getElementById("gunting2").style.background=
        "#C4C4C4";
        document.getElementById("kertas2").style.background=
        "#9C835F";
        document.getElementById("batu2").style.background=
        "#9C835F";
        } else {
        document.getElementById("kertas2").style.background=
        "#C4C4C4";
        document.getElementById("gunting2").style.background=
        "#9C835F";
        document.getElementById("batu2").style.background=
        "#9C835F";
    }

    console.log(`player: ${player}`);
    console.log(`com: ${com}`);
    console.log(`hasil: ${hasil}`);
    result()
  };

  const result = () => {
    let replace = document.querySelector('.replace');
    if (hasil === 'draw') {
        replace.innerHTML = 'DRAW';
        replace.classList.add('result');
        replace.style.background = '#035B0C';
        if (replace.classList.contains('versus')) {
        replace.classList.remove('versus');
        }
    } else if (hasil === 'player win') {
        replace.innerHTML = 'PLAYER 1 <br> WIN';
        replace.classList.add('result');
        replace.style.background = '#4C9654';
        if (replace.classList.contains('versus')) {
        replace.classList.remove('versus');
        }
    } else {
        replace.innerHTML = 'COM <br> WIN';
        replace.classList.add('result');
        replace.style.background = '#4C9654';
        if (replace.classList.contains('versus')) {
        replace.classList.remove('versus');
        }
    }
};


  const handleReload = (e) => {
    e.preventDefault();
    window.location.reload();
  };

  return (
    <>
      <div className="body">
        <Nav>
          <NavItem className="games p-4 ms-2" id="back">
            <a href="/home">
              <img src={back} alt="" />
            </a>
          </NavItem>
          <NavItem className="logo p-2 ms-2" id="logo">
            <img src={logo} alt="" />
          </NavItem>
          <NavItem className="headers p-4 ms-2">
            <h1>ROCK PAPER SCISSORS</h1>
          </NavItem>
        </Nav>
        <Container className="title">
          <Row className="user">
            <Col className="player p-3 d-flex">Player 1</Col>
            <Col className="com p-2 d-flex">COM</Col>
          </Row>
          <Row className="row batu">
            <Col className="col-4 d-flex">
              <div className="option" onClick={() => handlePicks("batu")} id="batu">
                <img src={imgbatu} alt="" />
              </div>
            </Col>
            <Col className="col-4"></Col>
            <Col className="col-4 d-flex">
              <div className="random" id="batu2">
                <img src={imgbatu} alt="" />
              </div>
            </Col>
          </Row>
          <Row className="row kertas">
            <Col className="col-4 d-flex">
              <div className="option" onClick={() => handlePicks("kertas")} id="kertas">
                <img src={imgkertas} alt="" />
              </div>
            </Col>
            <Col className="col-4 d-flex versus">
              <div className="replace d-flex">VS</div>
            </Col>
            <Col className="col-4 d-flex">
              <div className="random" id="kertas2">
                <img src={imgkertas} alt="" />
              </div>
            </Col>
          </Row>
          <Row className="row gunting">
            <Col className="col-4 d-flex">
              <div className="option" onClick={() => handlePicks("gunting")} id="gunting">
                <img src={imggunting} alt="" />
              </div>
            </Col>
            <Col className="col-4"></Col>
            <Col className="col-4 d-flex">
              <div className="random" id="gunting2">
                <img src={imggunting} alt="" />
              </div>
            </Col>
          </Row>

          <div className="refresh">
            <div className="col-md-6 offset-md-3">
              <img src={refresh} alt="" className="refresh-img" onClick={handleReload} />
            </div>
          </div>
        </Container>
      </div>
    </>
  );
}